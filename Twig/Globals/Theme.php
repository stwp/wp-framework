<?php

namespace Super\Twig\Globals;

class Theme
{
    /**
     * Get the URL th theme directory
     *
     * @param string $append
     *
     * @return string
     */
    public function url($append = '')
    {
        return trailingslashit(get_template_directory_uri()) . ltrim($append, '/');
    }

    /**
     * Get the Path to theme directory
     *
     * @param string $append
     *
     * @return string
     */
    public function dir($append = '')
    {
        return trailingslashit(get_template_directory()) . ltrim($append, '/');
    }

    /**
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed
     */
    public function mod($name, $default = false)
    {
        return get_theme_mod($name, $default);
    }
}
