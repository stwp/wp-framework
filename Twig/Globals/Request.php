<?php

namespace Super\Twig\Globals;

class Request
{
    public function get($key)
    {
        return $_GET[$key] ?: '';
    }

    public function post($key)
    {
        return $_POST[$key] ?: '';
    }

    public function cookie($key)
    {
        return $_COOKIE[$key] ?: '';
    }

    public function request($key)
    {
        return $_REQUEST[$key] ?: '';
    }
}
