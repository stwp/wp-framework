<?php

namespace Super\Twig\Globals;

class Site
{
    /**
     * @var string|void
     */
    public $admin_email;

    /**
     * @var string|void
     */
    public $name;

    /**
     * @var string|void
     */
    public $title;

    /**
     * @var string|void
     */
    public $description;

    /**
     * @var string|void
     */
    public $language;

    /**
     * @var string|void
     */
    public $charset;

    /**
     * @var string|void
     */
    public $pingback;

    /**
     * @var string|void
     */
    public $pingback_url;

    /**
     * Site constructor.
     */
    public function __construct()
    {
        $this->language     = get_bloginfo('language');
        $this->charset      = get_bloginfo('charset');
        $this->admin_email  = get_bloginfo('admin_email');
        $this->name         = get_bloginfo('name');
        $this->title        = $this->name;
        $this->description  = get_bloginfo('description');
        $this->pingback     = get_bloginfo('pingback_url');
        $this->pingback_url = $this->pingback;
    }

    /**
     * @param string $path
     * @param null   $scheme
     *
     * @return string|void
     */
    public function url($path = '', $scheme = null)
    {
        return home_url($path, $scheme);
    }

    /**
     * @return mixed|void
     */
    public function language_attributes()
    {
        return get_language_attributes();
    }

    /**
     * @param string $key
     *
     * @return string|void
     */
    public function info($key)
    {
        return get_bloginfo($key);
    }

    /**
     * @param string $option
     * @param mixed  $default
     *
     * @return mixed
     */
    public function option($option, $default = false)
    {
        return get_option($option, $default);
    }
}
