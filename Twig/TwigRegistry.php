<?php

namespace Super\Twig;

use Super\Registry\Registry;
use Super\Twig\Abstracts\AbstractFiltersRegistry;
use Super\Twig\Abstracts\AbstractFunctionsRegistry;

class TwigRegistry
{
    public static function addFunctionsToRegistry(AbstractFunctionsRegistry $registry)
    {
        Registry::add('twig-functions', sanitize_title(strtolower(get_class($registry))), $registry);
    }

    public static function addFiltersToRegistry(AbstractFiltersRegistry $registry)
    {
        Registry::add('twig-filters', sanitize_title(strtolower(get_class($registry))), $registry);
    }

}
