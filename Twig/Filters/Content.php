<?php

namespace Super\Twig\Filters;

use Super\Twig\Abstracts\AbstractFiltersRegistry;

class Content extends AbstractFiltersRegistry
{
    public function __construct()
    {
        $this->add('site_content_inner', [$this, 'site_content_inner'], [
            'is_safe' => ['html'],
        ]);
    }

    public function site_content_inner($string)
    {
        $filteredString = preg_replace(
            '/<section class="site-container super-container--space-normal">(\s)*<\/section>/',
            '',
            $string
        );

        if ($filteredString === null) {
            return $string;
        }

        return $filteredString;
    }
}
