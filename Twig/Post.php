<?php

namespace Super\Twig;

use Super\Exceptions\InvalidTaxonomyName;
use Super\Interfaces\ElementInterface;
use Super\Interfaces\MetaGetterInterface;
use Twig\Markup;
use WP_Error;
use WP_Post;
use WP_Term;

class Post implements ElementInterface, MetaGetterInterface
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var array|WP_Post|null
     */
    public $post;

    /**
     * @var string
     */
    public $title;

    /**
     * @var false|string
     */
    public $url;

    /**
     * @var false|string
     */
    public $date;

    /**
     * @var string
     */
    public $content;

    /**
     * @var null|[]
     */
    protected $terms = null;

    /**
     * Post constructor.
     *
     * @param $id
     */
    public function __construct($id)
    {
        $this->post    = WP_Post::get_instance($id);
        $this->id      = $this->post->ID;
        $this->title   = $this->title();
        $this->url     = $this->url();
        $this->date    = $this->date();
        $this->content = $this->content();
    }

    public function id()
    {
        return absint($this->id);
    }

    public function title()
    {
        return $this->post->post_title;
    }

    public function url()
    {
        return get_the_permalink($this->id);
    }

    public function date($dateFormat = '')
    {
        $id = !empty($this->id) ? $this->id : null;

        return get_the_date($dateFormat, $id);
    }

    public function content()
    {
        $content = apply_filters('the_content', $this->post->post_content);
        $content = str_replace(']]>', ']]&gt;', $content);

        return new Markup($content, 'UTF-8');
    }

    public function terms($taxonomy)
    {
        // Maybe get the terms from cache
        if (isset($this->terms[$taxonomy])) {
            return $this->terms[$taxonomy];
        }

        // Get the terms
        $terms = get_the_terms($this->id, $taxonomy);

        try {
            // We must have an array to work with.
            if (is_wp_error($terms)) {
                /** @var WP_Error $error */
                $error = $terms;

                $terms = [];

                throw new InvalidTaxonomyName($error->get_error_message());
            }
            if (is_wp_error($terms) || !is_array($terms)) {
                $terms = [];
            }
        }
        catch (InvalidTaxonomyName $e) {
            echo $e->getMessage();
        }

        if (empty($terms)) {
            return [];
        }

        $response = [];

        /** @var WP_Term $term */
        foreach ($terms as $term) {
            $response[] = new Term($term);
        }

        // Save in cache
        $this->terms[$taxonomy] = $response;

        return $response;
    }

    public function categories()
    {
        return $this->terms('category');
    }

    public function tags()
    {
        return $this->terms('post_tag');
    }

    /**
     * Get post meta
     *
     * @param $key
     *
     * @return mixed
     */
    public function meta($key)
    {
        return get_post_meta($this->id, $key, true);
    }

    /**
     * Alias
     *
     * @param $key
     *
     * @return mixed
     */
    public function get_field($key)
    {
        return $this->meta($key);
    }

    /**
     * @param bool $allData Show all data about block or only the blockID
     *
     * @return array
     */
    public function blocks($allData = false)
    {
        if (has_blocks($this->post->post_content)) {
            $blocks = parse_blocks($this->post->post_content);

            if (empty($allData)) {
                return $blocks;
            }

            return array_filter(array_column($blocks, 'blockName'));
        }

        return [];
    }

    /**
     * @return array
     */
    public function blocksData()
    {
        return $this->blocks(true);
    }
}
