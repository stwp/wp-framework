<?php

namespace Super\Twig\Functions;

use enshrined\svgSanitize\Sanitizer;
use Super\Support\Svg\Attributes;
use Super\Support\Svg\Tags;
use Super\Twig\Abstracts\AbstractFunctionsRegistry;
use Timber\Timber;

class Helpers extends AbstractFunctionsRegistry
{
    public function __construct()
    {
        $this->add('dd', [$this, 'dd']);
        $this->add('pre', [$this, 'pre']);
        $this->add('super_svg', [$this, 'super_svg']);
        $this->add('super_attachment', [$this, 'super_attachment']);
        $this->add('super_img', [$this, 'super_img']);
        $this->add('split_by_newline', [$this, 'split_by_newline']);
    }

    /**
     * Debug helper.
     *
     * @param mixed $value
     */
    public function dd($value)
    {
        echo '<pre>';
        print_r(esc_html(var_export($value, true)) . PHP_EOL);
        echo '</pre>';
    }

    /**
     * Pre helper.
     *
     * @param mixed $value
     * @param bool  $trim
     */
    public function pre($value, $trim = true)
    {
        if ($trim) {
            $value = trim($value);
        }

        echo '<pre>';
        print_r(esc_html($value) . PHP_EOL);
        echo '</pre>';
    }

    /**
     * Get the SVG source by attachment ID.
     *
     * @param      $image_object_or_id
     *
     * @param bool $return_array
     *
     * @return false|string|array
     */
    public function super_svg($image_object_or_id, $return_array = false)
    {
        $img_id = false;

        if (is_numeric($image_object_or_id)) {
            $img_id = absint($image_object_or_id);
        }

        if (!empty($image_object_or_id->id)) {
            $img_id = absint($image_object_or_id->id);
        }

        if (empty($img_id)) {
            return '';
        }

        $file = get_attached_file($img_id);
        //$file_url = wp_get_attachment_image_url($img_id, 'full');
        $ext     = pathinfo($file, PATHINFO_EXTENSION);
        $content = !empty($file) && $ext == 'svg' ? file_get_contents($file) : '';
        $content = $this->sanitizeSvg($content);

        if ($return_array) {
            return [
                'id'      => $img_id,
                'ext'     => $ext,
                'file'    => $file,
                'content' => $content,
            ];
        }

        $content = !empty($content) ? $content : '';

        return $this->sanitizeSvg($content);
    }

    /**
     * @param           $image_path_or_id
     * @param array|int $width_or_settings
     *
     * @return false|string
     */
    public function super_img($image_path_or_id, $width_or_settings = null)
    {
        if (is_numeric($image_path_or_id)) {
            $id = absint($image_path_or_id);
        }
        else {
            $id = strpos($image_path_or_id, 'wp-content/uploads/') === false
                ? wp_normalize_path(SUPER_DIR . $image_path_or_id)
                : $image_path_or_id;
        }

        if (!is_array($width_or_settings)) {
            return $this->super_attachment($id, $width_or_settings);
        }

        $settings = wp_parse_args($width_or_settings, [
            'width'  => null,
            'height' => 0,
            'crop'   => 'default',
            'force'  => false,
        ]);

        extract($settings);

        /**
         * @var int    $width
         * @var int    $height
         * @var string $crop
         * @var bool   $force
         */
        return $this->super_attachment($id, $width, $height, $crop, $force);
    }

    /**
     * Get the SVG source or the img depending on the source.
     *
     * @param          $image_path_or_id
     *
     * @param null|int $width
     * @param int      $height
     * @param string   $crop
     * @param bool     $force
     *
     * @return false|string
     */
    public function super_attachment($image_path_or_id, $width = null, $height = 0, $crop = 'default', $force = false)
    {
        // It's a string. Most likely to be a direct url or path.
        if (is_string($image_path_or_id) && !is_numeric($image_path_or_id)) {
            $ext = pathinfo($image_path_or_id, PATHINFO_EXTENSION);

            // Get the contents if it's a SVG.
            if ($ext === 'svg') {
                // Transform the URL to path if it's from uploads directory
                if (strpos($image_path_or_id, 'wp-content/uploads/') !== false) {
                    $split        = explode('wp-content/', $image_path_or_id);
                    $svg_img_path = wp_normalize_path(trailingslashit(WP_CONTENT_DIR) . $split[1]);

                    $content = file_get_contents($svg_img_path);

                    return $this->sanitizeSvg($content);
                }

                // At this time it should be a path(from theme root maybe)
                $content = file_get_contents($image_path_or_id);

                return $this->sanitizeSvg($content);
            }
            // It's a normal image
            else if (in_array($ext, ['jpg', 'jpeg', 'gif', 'png'])) {
                $img = $image_path_or_id;

                // Transform path to url if it's from theme directory.
                if (strpos($img, SUPER_DIR) !== false) {
                    $img = str_ireplace(SUPER_DIR, SUPER_URL, $img);
                }

                // Resize it only if it's from uploads directory
                if ($width > 0 && strpos($img, 'wp-content/uploads/') !== false) {
                    $img = \Timber\ImageHelper::resize($img, $width, $height, $crop, $force);
                }

                return '<img src="' . $img . '" alt="" />';
            }
        }

        // Maybe it's an object or an ID?
        $image_array = $this->super_svg($image_path_or_id, true);

        if (!empty($image_array['content'])) {
            return $image_array['content'];
        }

        else if (!empty($image_array['ext']) && in_array($image_array['ext'], ['jpg', 'jpeg', 'gif', 'png'])) {
            $img_data = wp_prepare_attachment_for_js($image_array['id']);
            $img      = $img_data['url'];

            if ($width > 0 && strpos($img, 'wp-content/uploads/') !== false) {
                $img = \Timber\ImageHelper::resize($img, $width, $height, $crop, $force);
            }

            return '<img src="' . $img . '" alt="' . $img_data['alt'] . '" />';
        }

        return '';
    }

    /**
     * Explode a text by new line character and return the array.
     *
     * @param string      $text
     * @param null|string $tag
     *
     * @return array[]|string
     */
    public function split_by_newline($text, $tag = null)
    {
        $items = array_filter(preg_split('/\r\n|\r|\n/', $text));

        if (empty($items)) {
            return [];
        }

        if (!empty($tag) && is_string($tag)) {
            $items = array_map(function ($value) use ($tag) {
                return "<{$tag}>{$value}</{$tag}>";
            }, $items);

            return join(PHP_EOL, $items);
        }

        return $items;
    }

    protected function sanitizeSvg($content)
    {
        $sanitizer = new Sanitizer();
        $sanitizer->setAllowedTags(new Tags());
        $sanitizer->setAllowedAttrs(new Attributes());

        $sanitizer->minify(true);
        $sanitizer->removeXMLTag(true);

        return $sanitizer->sanitize($content);
    }
}
