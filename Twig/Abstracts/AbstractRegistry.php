<?php

namespace Super\Twig\Abstracts;

use Super\Interfaces\DoneInterface;

abstract class AbstractRegistry implements DoneInterface
{
    /**
     * @var array
     */
    protected $tags = [];

    public function done()
    {
        add_filter('get_twig', [$this, 'add_to_twig']);
    }

    public function add_to_twig($twig)
    {
        // Register functions as twig helpers
        foreach ($this->tags as $tag => $opt) {
            $this->register(
                $twig,
                $tag,
                $opt['function'],
                $opt['options']
            );
        }

        return $twig;
    }

    protected function add($name, $function, $options = [])
    {
        $this->tags[$this->snake($name)] = [
            'function' => $function,
            'options'  => $options,
        ];
    }

    abstract protected function register($twig, $name, $function, $options = []);

    /**
     * Convert a string to snake case
     *
     * @param string $value
     * @param string $delimiter
     *
     * @return string
     */
    protected function snake($value, $delimiter = '_')
    {
        if (!ctype_lower($value)) {
            $value = strtolower(preg_replace('/(.)(?=[A-Z])/u', '$1' . $delimiter, $value));
        }

        return $value;
    }
}
