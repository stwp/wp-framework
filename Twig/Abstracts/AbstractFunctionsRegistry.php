<?php

namespace Super\Twig\Abstracts;

use Super\Interfaces\DoneInterface;
use Twig\TwigFunction;

abstract class AbstractFunctionsRegistry extends AbstractRegistry implements DoneInterface
{
    protected function register($twig, $name, $function, $options = [])
    {
        $twig->addFunction(
            new TwigFunction($this->snake($name), $function, $options)
        );
    }

}
