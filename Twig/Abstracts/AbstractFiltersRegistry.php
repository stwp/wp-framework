<?php

namespace Super\Twig\Abstracts;

use Super\Interfaces\DoneInterface;
use Twig\TwigFilter;

abstract class AbstractFiltersRegistry extends AbstractRegistry implements DoneInterface
{
    protected function register($twig, $name, $function, $options = [])
    {
        $twig->addFilter(
            new TwigFilter($this->snake($name), $function, $options)
        );
    }

}
