<?php

namespace Super\Twig;

use Super\Interfaces\ElementInterface;
use Super\Interfaces\MetaGetterInterface;
use Twig\Markup;
use WP_Term;

class Term implements ElementInterface, MetaGetterInterface
{
    public $term;

    public $id;

    public $title;

    public $url;

    public $slug;

    public $group;

    public $taxonomy_id;

    public $taxonomy;

    public $description;

    public $parent;

    public $count;

    /**
     * Term constructor.
     *
     * @param WP_Term $term
     */
    public function __construct(WP_Term $term)
    {
        $this->term        = $term;
        $this->id          = $this->id();
        $this->title       = $this->title();
        $this->url         = $this->url();
        $this->slug        = $this->slug();
        $this->group       = $this->group();
        $this->taxonomy_id = $this->taxonomy_id();
        $this->taxonomy    = $this->taxonomy();
        $this->description = $this->description();
        $this->parent      = $this->parent();
        $this->count       = $this->count();
    }

    public function id(): int
    {
        return absint($this->term->term_id);
    }

    public function title(): string
    {
        return $this->term->name;
    }

    public function url()
    {
        return get_term_link($this->id(), $this->taxonomy);
    }

    public function taxonomy_id(): int
    {
        return absint($this->term->term_taxonomy_id);
    }

    public function taxonomy(): string
    {
        return $this->term->taxonomy;
    }

    public function slug(): string
    {
        return $this->term->slug;
    }

    public function group()
    {
        return $this->term->term_group;
    }

    public function description(): string
    {
        return new Markup($this->term->description, 'UTF-8');
    }

    public function parent(): int
    {
        return absint($this->term->parent);
    }

    public function count(): int
    {
        return absint($this->term->count);
    }

    /**
     * Get post meta
     *
     * @param $key
     *
     * @return mixed
     */
    public function meta($key)
    {
        return get_term_meta($this->id, $key, true);
    }

    /**
     * Alias
     *
     * @param $key
     *
     * @return mixed
     */
    public function get_field($key)
    {
        return $this->meta($key);
    }
}
