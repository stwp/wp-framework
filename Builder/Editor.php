<?php

namespace Super\Builder;

use FilesystemIterator;
use Timber\Timber;
use WP_Block_Type_Registry;

class Editor
{
    public $blocksPath = 'views/blocks';

    public $blocks = [];

    public function __construct()
    {
        $this->blocks = $this->getBlocks();
    }

    public function init()
    {
        add_action('enqueue_block_editor_assets', [$this, 'block_editor_assets']);
        add_action('enqueue_block_editor_assets', [$this, 'block_assets']);

        add_filter('block_categories', [$this, 'add_block_categories']);
        add_filter('block_editor_settings', [$this, 'autosave_value']);

        add_action('init', [$this, 'inject']);

        add_filter('content_save_pre', [$this, 'clearEmptyParagraphs'], 10, 1);
    }

    public function inject()
    {
        $this->unset_default_blocks();
        $this->include_blocks();
    }

    public function block_editor_assets()
    {
        wp_register_script(
            'super-builder-metaboxes',
            SUPER_URL . 'js/meta.js',
            [
                'wp-plugins',
                'wp-edit-post',
                'wp-element',
                'wp-components',
            ],
            filemtime(SUPER_DIR . 'js/meta.js'),
            true
        );

        wp_enqueue_script(
            'super-blocks',
            SUPER_URL . 'js/blocks.js',
            ['wp-blocks', 'wp-i18n', 'wp-element', 'underscore'],
            filemtime(SUPER_DIR . 'js/blocks.js'),
            true
        );
    }

    public function block_assets()
    {
        wp_enqueue_style(
            'super-blocks',
            SUPER_URL . 'css/blocks.css',
            [],
            filemtime(SUPER_DIR . 'css/blocks.css')
        );
    }

    public function include_blocks()
    {
        if (!empty($this->blocks)) {
            $priority = has_filter('the_content', 'wpautop');
            remove_filter('the_content', 'wpautop', $priority);

            foreach ($this->blocks as $block) {
                $block_name = basename($block);

                register_block_type('super/' . $block_name, [
                    'render_callback' => [__CLASS__, 'block_callback'],
                ]);
            }

            add_filter('the_content', '_restore_wpautop_hook', $priority + 1);
        }
    }

    public static function block_callback($attributes, $innerBlocks)
    {
        if (!isset($attributes['_block_name'])) {
            return '';
        }

        $context = Timber::get_context();

        if (!empty($attributes)) {
            foreach ($attributes as $key => $attribute) {
                if (self::is_json($attribute)) {
                    $attributes[$key] = json_decode($attribute);
                }
            }
        }

        $name = str_replace('super/', '', $attributes['_block_name']);

        $attributes['_block_type'] = $name;

        $innerBlocks = trim($innerBlocks);
        $innerBlocks = preg_replace('/<div class="wp-block-super-group(.*)">/', '', $innerBlocks, 1);
        $innerBlocks = rtrim($innerBlocks, '</div>');
        $innerBlocks = trim($innerBlocks);

        $context['innerBlocks'] = $innerBlocks;

        $className = Config::getBlockConfigClass($attributes);

        if (!empty($className)) {
            /* @var AbstractConfigLoader $class */
            $class = new $className($attributes);
            $class->data();
            $attributes = $class->getAttributes();
        }

        $context['super'] = $attributes;

        $priority = has_filter('the_content', 'wpautop');
        remove_filter('the_content', 'wpautop', $priority);
        $output = Timber::fetch('blocks/' . $name . '/view.twig', $context);
        add_filter('the_content', '_restore_wpautop_hook', $priority + 1);

        return $output;
    }

    public function unset_default_blocks()
    {
        $block_types = array_keys(WP_Block_Type_Registry::get_instance()->get_all_registered());
        foreach ($block_types as $block) {
            if (strpos($block, 'super') === false) {
                unregister_block_type($block);
            }
        }
    }

    public function add_block_categories($categories)
    {
        return array_merge($categories, [
            [
                'slug'  => 'super',
                'title' => __('Super Elements', 'super'),
            ],
        ]);
    }

    public function autosave_value($settings)
    {
        $settings['autosaveInterval'] = 99999999999;

        return $settings;
    }

    public static function is_json($item)
    {
        if (is_string($item)) {
            json_decode($item);

            return json_last_error() == JSON_ERROR_NONE;
        }

        return false;
    }

    public function getBlocks()
    {
        $flags    = FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::SKIP_DOTS;
        $iterator = new FilesystemIterator(SUPER_DIR . $this->blocksPath, $flags);
        $files    = [];

        foreach ($iterator as $path => $item) {
            if ($item->isDir()) {
                $files[] = $path;
            }
        }

        return $files;
    }

    public function clearEmptyParagraphs($content)
    {
        $content = preg_replace('/<!--\swp:paragraph\s-->(\s*\v+)<p><\/p>(\s*\v+)<!--\s\/wp:paragraph\s-->/i', '', $content);

        return $content;
    }
}
