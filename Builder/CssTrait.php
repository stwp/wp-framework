<?php

namespace Super\Builder;

trait CssTrait
{

    protected $css = [];

    /**
     * Add the CSS to head
     */
    public function initCss()
    {
        $this->globalOptionsCss();
        $this->customCss();
    }

    /**
     * Apply CSS rules to an element related to this block.
     *
     * @param      $selector
     * @param      $cssRules
     * @param bool $mediaQuery
     *
     * @return $this
     */
    public function css($selector, $cssRules, $mediaQuery = false)
    {
        $selector = $this->sanitizeSelector($selector);

        if ($mediaQuery) {
            $this->css[$mediaQuery][$selector][] = $cssRules;
        }
        else {
            $this->css['allMediaQueries'][$selector][] = $cssRules;
        }

        return $this;
    }

    /**
     * Apply CSS rules directly to this block container.
     *
     * @param      $cssRules
     * @param bool $mediaQuery
     *
     * @return CssTrait
     */
    public function sectionCss($cssRules, $mediaQuery = false)
    {
        return $this->css('&', $cssRules, $mediaQuery);
    }

    /**
     * Compile the CSS.
     */
    public function buildCss()
    {
        $output = '';

        foreach ($this->css as $mediaQuery => $css) {
            $compiledCss = '';

            foreach ($css as $selector => $rules) {
                $realSelector = str_ireplace('[unique_block_class]', '.super-block--' . $this->get('_unique_block_id'), $selector);

                $cssRules = [];

                foreach ($rules as $caseRules) {
                    if (is_array($caseRules)) {
                        foreach ($caseRules as $key => $value) {
                            $cssRules[] = "{$key}: {$value}";
                        }
                    }
                    else {
                        $cssRules[] = $caseRules;
                    }
                }

                $joinedRules = join(';', $cssRules);

                $compiledCss .= "{$realSelector}{ {$joinedRules} }";
            }

            if ('allMediaQueries' !== $mediaQuery) {
                $output .= "@media({$mediaQuery}){ {$compiledCss} }";
            }
            else {
                $output .= $compiledCss;
            }
        }

        echo $output;
    }

    /**
     * Make sure that we have a valid selector that applies only to this section and/or its child elements
     *
     * @param string $selector
     *
     * @return string
     */
    public function sanitizeSelector($selector)
    {
        // Remove white spaces from start and end
        $selector = trim($selector);

        // If the selector is empty, then just apply styles directly to the section.
        if (empty($selector)) {
            return '[unique_block_class]';
        }

        // Back compatibility(when [unique_block_class] was used). Convert to ampersand to make the following checks possible.
        $selector = str_replace('[unique_block_class]', '&', $selector);

        // If the selector contains at least one ampersand, then just replace all of them(&) with the uniques block classname.
        if (strpos($selector, '&') !== false) {
            return str_replace('&', '[unique_block_class]', $selector);
        }

        // If the selector DOES NOT contain an ampersand, then add the unique classname at the start followed by a space.
        if (strpos($selector, '&') === false) {
            return "[unique_block_class] {$selector}";
        }

        return $selector;
    }

    /**
     * Global options have custom CSS
     */
    protected function globalOptionsCss()
    {
        // Top container padding
        if ($this->get('_topSpace') === 'custom') {
            $this->css('& .super-container--top-space-custom', [
                'padding-top' => ($this->get('_customTopSpace') ?: 0) . ($this->get('_customTopSpaceUnit') ?: 'px'),
            ]);
        }

        // Bottom container padding
        if ($this->get('_bottomSpace') === 'custom') {
            $this->css('& .super-container--bottom-space-custom', [
                'padding-bottom' => ($this->get('_customBottomSpace') ?: 0) . ($this->get('_customBottomSpaceUnit') ?: 'px'),
            ]);
        }

        // Text color
        if (!empty($this->get('_textColor'))) {
            $this->sectionCss([
                'color' => $this->get('_textColor'),
            ]);
        }

        // Background image
        if (!empty($this->get('_backgroundImage')['url'])) {
            $this->sectionCss([
                'background-image' => "url(\"{$this->get('_backgroundImage')['url']}\")",
            ]);
        }

        // Background position
        if (!empty($this->get('_backgroundPosition')) && $this->get('_backgroundPosition') !== 'custom') {
            $this->sectionCss([
                'background-position' => $this->get('_backgroundPosition'),
            ]);
        }
        else if ($this->get('_backgroundPosition') === 'custom' && !empty($this->get('_customBackgroundPosition'))) {
            $this->sectionCss([
                'background-position' => $this->get('_customBackgroundPosition'),
            ]);
        }

        // Background size
        if (!empty($this->get('_backgroundSize')) && $this->get('_backgroundSize') !== 'custom') {
            $this->sectionCss([
                'background-size' => $this->get('_backgroundSize'),
            ]);
        }
        else if ($this->get('_backgroundSize') === 'custom' && !empty($this->get('_customBackgroundSize'))) {
            $this->sectionCss([
                'background-size' => $this->get('_customBackgroundSize'),
            ]);
        }

        // Background color
        if (!empty($this->get('_backgroundColor'))) {
            $this->sectionCss([
                'background-color' => $this->get('_backgroundColor'),
            ]);
        }
    }
}
