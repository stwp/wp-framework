<?php

namespace Super\Builder;

abstract class AbstractConfigLoader
{
    use CssTrait;

    protected $options = [];

    public function __construct($attrs)
    {
        $this->options = $attrs;
    }

    public function enqueue()
    {
    }

    public function customCss()
    {
    }

    public function data()
    {
    }

    /**
     * Get a single option from the saved block attributes
     *
     * @param string $option
     *
     * @return mixed|null
     */
    public function get($option)
    {
        return $this->getFrom($option, $this->options);
    }

    /**
     * Magic properties. ;)
     *
     * @param $name
     *
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->get($name);
    }

    /**
     * Get a value by key from an array
     *
     * @param string $option
     * @param array  $from
     *
     * @return mixed|null
     */
    public function getFrom($option, $from)
    {
        if (is_object($from)) {
            $value = isset($from->$option) ? $from->$option : null;
        }
        else {
            $value = isset($from[$option]) ? $from[$option] : null;
        }

        $value = $this->maybeJsonDecode($value);

        return $value;
    }

    /**
     * Change a single option in the saved block attributes
     *
     * @param string $option
     * @param mixed  $value
     *
     * @return void
     */
    public function set($option, $value)
    {
        $this->options[$option] = $value;
    }

    /**
     * Get all attributes. They may or may not be modified when calling this method.
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->options;
    }

    /**
     * Test if given object is a JSON string or not.
     *
     * @param mixed $obj
     *
     * @return bool
     */
    public function isJson($obj)
    {
        return is_string($obj)
            && is_array(json_decode($obj, true))
            && json_last_error() === JSON_ERROR_NONE;
    }

    /**
     * Maybe JSON decode the given string.
     *
     * @param string $str
     * @param bool   $assoc
     *
     * @return mixed
     */
    public function maybeJsonDecode($str, $assoc = false)
    {
        return $this->isJson($str) ? json_decode($str, $assoc) : $str;
    }
}
