<?php

namespace Super\Builder;

use WP_Post;

class Config
{
    public function init()
    {
        add_action('wp_enqueue_scripts', [$this, 'assetsForActiveBlocks'], 1);
        add_action('super_theme_custom_css', [$this, 'customCssForActiveBlocks']);
    }

    public function assetsForActiveBlocks()
    {
        // Block specific scripts
        $this->blocksIterator('enqueue');

        $activeBlocks = $this->blocks(get_post());

        $activeBlocksNames = [];
        if (!empty($activeBlocks)) {
            $activeBlocksNames = array_filter(array_column($activeBlocks, 'blockName'));
        }

        // Additional data passed to 'main.js
        echo '<script>';
        echo "var superMainData = " . wp_json_encode([
                'active_blocks' => $activeBlocksNames,
            ]) . ';';
        echo '</script>';
    }

    public function customCssForActiveBlocks()
    {
        $this->blocksIterator('buildCss');
    }

    public function blocksIterator($callback)
    {
        $currentPost = get_post();
        $blocks      = $this->blocks($currentPost);

        foreach ($blocks as $block) {
            if (empty($block['blockName'])) {
                continue;
            }
            if (empty($block['attrs'])) {
                continue;
            }

            $className = static::getBlockConfigClass($block['blockName']);

            if (empty($className)) {
                continue;
            }

            /* @var AbstractConfigLoader $class */
            $class = new $className($block['attrs']);
            $class->initCss();

            $class->$callback();
        }
    }

    /**
     * @param WP_Post $post
     * @param bool    $allData Show all data about block or only the blockID
     *
     * @return array
     */
    public function blocks($post, $allData = true)
    {
        if (empty($post->post_content)) {
            return [];
        }

        if (has_blocks($post->post_content)) {
            $blocks = parse_blocks($post->post_content);
            $blocks = $this->parseBlocks($blocks);

            if (!empty($allData)) {
                return $blocks;
            }

            $blocks = array_filter(array_column($blocks, 'blockName'));

            return $blocks;
        }

        return [];
    }

    protected function parseBlocks($blocks)
    {
        $allBlocks = [];

        foreach ($blocks as $block) {
            if (!empty($block['innerBlocks'])) {
                $innerBlocks = $block['innerBlocks'];
                unset($block['innerBlocks']);
                $allBlocks = array_merge($allBlocks, $this->parseBlocks($innerBlocks));
            }

            $allBlocks[] = $block;
        }

        return $allBlocks;
    }

    public static function getBlockConfigClass($blockName)
    {
        if (is_array($blockName) && !empty($blockName['_block_name'])) {
            $blockName = $blockName['_block_name'];
        }

        if (!is_string($blockName)) {
            return null;
        }

        $blockName = str_replace('super/', '', $blockName);
        $blockName = ucwords(str_replace(['-', '_'], ' ', $blockName));
        $blockName = trim(str_replace(' ', '', $blockName));

        $path = SUPER_DIR . "app/Blocks/{$blockName}/Config.php";

        if (!file_exists($path)) {
            return null;
        }

        return "\\App\\Blocks\\{$blockName}\\Config";
    }
}
