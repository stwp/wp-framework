<?php

namespace Super\Builder;

class BlockShapes
{
    public function init()
    {
        add_action('super_block_top', [$this, 'topShape'], 99);
        add_action('super_block_bottom', [$this, 'bottomShape'], 99);
    }

    public function topShape($attrs)
    {
        $this->shapeGenerator($attrs, 'top');
    }

    public function bottomShape($attrs)
    {
        $this->shapeGenerator($attrs, 'bottom');
    }

    protected function shapeGenerator($attrs, $position)
    {
        if (empty($attrs["_{$position}Shape"])) {
            return;
        }

        $content = file_get_contents(SUPER_DIR . "media/shapes/" . $attrs["_{$position}Shape"] . ".svg");

        if (empty($content)) {
            return;
        }

        $style = [];

        if (!empty($attrs["_{$position}ShapeZoom"]) && absint($attrs["_{$position}ShapeZoom"]) > 100) {
            $style[] = "width: {$attrs["_{$position}ShapeZoom"]}%";
        }

        if (!empty($attrs["_{$position}ShapeBackgroundColor"])) {
            $style[] = "color: {$attrs["_{$position}ShapeBackgroundColor"]}";
        }

        if (!empty($attrs["_{$position}ShapeHeight"]) && absint($attrs["_{$position}ShapeHeight"]) >= 0) {
            $style[] = "max-height: {$attrs["_{$position}ShapeHeight"]}px";
        }

        $style      = join(';', $style);
        $shapeClass = sanitize_html_class($attrs["_{$position}Shape"]);

        echo "<div class='super-block-shape-{$position} super-block-shape-{$position}--{$shapeClass}' style='{$style}'>{$content}</div>";
    }
}
