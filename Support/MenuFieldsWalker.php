<?php

namespace Super\Support;

use Super\Registry\MenuFields;

if (!class_exists('\Walker_Nav_Menu_Edit')) {
    require_once ABSPATH . 'wp-admin/includes/nav-menu.php';
}

class MenuFieldsWalker extends \Walker_Nav_Menu_Edit
{
    function start_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {
        $item_output = '';
        parent::start_el($item_output, $item, $depth, $args);

        if ($field = MenuFields::getFormattedFields($item)) {
            $item_output = preg_replace('/(?=<fieldset.+class="[^"]*description.+description-wide)/'
                , $field, $item_output);
        }

        $output .= $item_output;
    }
}
