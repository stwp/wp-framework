<?php

namespace Super\Support\Svg;

use enshrined\svgSanitize\data\AllowedTags;

class Tags extends AllowedTags
{

    /**
     * Returns an array of tags
     *
     * @return array
     */
    public static function getTags()
    {
        /**
         * var  array Tags that are allowed.
         */
        return apply_filters('svg_allowed_tags', parent::getTags());
    }
}
