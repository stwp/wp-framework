<?php

namespace Super\Support\Svg;

use enshrined\svgSanitize\data\AllowedAttributes;

class Attributes extends AllowedAttributes
{

    /**
     * Returns an array of attributes
     *
     * @return array
     */
    public static function getAttributes()
    {
        $attrs = parent::getAttributes();

        $attrs = array_filter($attrs, function($value){
            return !in_array($value, ['id', 'height', 'width']);
        });
        
        return apply_filters('svg_allowed_attributes', $attrs);
    }
}
