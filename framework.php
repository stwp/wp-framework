<?php
/**
 * Framework entry point
 */

use Super\Builder\BlockShapes;
use Super\Builder\Config;
use Super\Builder\Editor;
use Super\Interfaces\DoneInterface;
use Super\Loader;
use Super\Registry\Registry;
use Super\Support\Svg\Svg;
use Super\Support\ThemeSetup;
use Super\Twig\TwigRegistry;
use Timber\Timber;

/*
-------------------------------------------------------------------------------
Constants
-------------------------------------------------------------------------------
*/
// Theme base directory
define('SUPER_DIR', trailingslashit(get_template_directory()));

// Theme base directory URL.
define('SUPER_URL', trailingslashit(get_template_directory_uri()));

// Framework directory
define('SUPER_FRAMEWORK_DIR', SUPER_DIR . 'wp-framework/');

// Views directory
define('SUPER_VIEWS_DIR', SUPER_DIR . 'views/');

/*
-------------------------------------------------------------------------------
Autoloader
-------------------------------------------------------------------------------
*/

require_once SUPER_FRAMEWORK_DIR . 'Loader/Loader.php';
require_once SUPER_FRAMEWORK_DIR . 'Loader/LoadClasses.php';
require_once SUPER_FRAMEWORK_DIR . 'Loader/LoadFiles.php';

try {
    Loader::loadClasses(SUPER_FRAMEWORK_DIR, 'Super');
}
catch (Exception $e) {
    echo $e->getTraceAsString();
}

/*
-------------------------------------------------------------------------------
SVG Support
-------------------------------------------------------------------------------
*/
new Svg();
new ThemeSetup();

/*
-------------------------------------------------------------------------------
Twig
-------------------------------------------------------------------------------
*/

// Init
// ----------------------------------------------------------------------------
$timber = new Timber();

Timber::$dirname = ['views', 'wp-base/views'];

// Register custom functions for twig
// ----------------------------------------------------------------------------
$twigFunctions = glob(SUPER_FRAMEWORK_DIR . 'Twig/Functions/*.php');
$twigFunctions = array_map(function ($value) {
    $className = str_ireplace('.php', '', basename($value));

    return "Super\\Twig\\Functions\\{$className}";
}, $twigFunctions);

$twigFunctions = apply_filters('super_twig_functions_registry', $twigFunctions);

foreach ($twigFunctions as $twigFunctionClass) {
    TwigRegistry::addFunctionsToRegistry(new $twigFunctionClass());
}

// Register custom filters for twig
// ----------------------------------------------------------------------------
$twigFilters = glob(SUPER_FRAMEWORK_DIR . 'Twig/Filters/*.php');
$twigFilters = array_map(function ($value) {
    $className = str_ireplace('.php', '', basename($value));

    return "Super\\Twig\\Filters\\{$className}";
}, $twigFilters);

$twigFilters = apply_filters('super_twig_filters_registry', $twigFilters);

foreach ($twigFilters as $twigFilterClass) {
    TwigRegistry::addFiltersToRegistry(new $twigFilterClass());
}

// Register custom globals for twig
// ----------------------------------------------------------------------------

add_filter('get_twig', function ($twig) {
    $twigGlobals = glob(SUPER_FRAMEWORK_DIR . 'Twig/Globals/*.php');
    $twigGlobals = array_map(function ($value) {
        $className = str_ireplace('.php', '', basename($value));

        return [
            strtolower(sanitize_title($className)) => "Super\\Twig\\Globals\\{$className}",
        ];
    }, $twigGlobals);

    $twigGlobals = apply_filters('super_twig_globals_registry', $twigGlobals);

    foreach ($twigGlobals as $global) {
        foreach ($global as $tagname => $class) {
            $twig->addGlobal($tagname, new $class());
        }
    }

    return $twig;
});

/*
-------------------------------------------------------------------------------
Editor blocks init
-------------------------------------------------------------------------------
*/
$editor = new Editor();
$editor->init();

function super_builder_init()
{
    $editorConfig = new Config();
    $editorConfig->init();

    $blockShapes = new BlockShapes();
    $blockShapes->init();
}

add_action('init', 'super_builder_init');

/*
-------------------------------------------------------------------------------
Components
-------------------------------------------------------------------------------
*/
function super_build()
{
    $all = Registry::all();

    foreach ($all as $registryType => $registry) {
        /** @var $obj DoneInterface */
        foreach ($registry as $registryId => $obj) {
            $obj->done();
        }
    }
}
