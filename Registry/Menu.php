<?php

namespace Super\Registry;

use Super\Interfaces\DoneInterface;
use Timber\Menu as TimberMenu;

class Menu implements DoneInterface
{
    /**
     * @var string
     */
    protected $menuId;

    /**
     * @var string
     */
    protected $title;

    public function __construct($menuId, $title)
    {
        $this->menuId = $menuId;
        $this->title  = $title;

        Registry::add('menus', $this->menuId, $this);
    }

    public function register()
    {
        register_nav_menu($this->menuId, $this->title);
    }

    /**
     * @param $context
     *
     * @return mixed
     */
    public function add_to_context($context)
    {
        if (!isset($context['menus'])) {
            $context['menus'] = [];
        }

        $context['menus'][$this->menuId] = new TimberMenu($this->menuId);

        return $context;
    }

    public function done()
    {
        add_action('after_setup_theme', [$this, 'register']);
        add_filter('timber_context', [$this, 'add_to_context']);
    }
}
