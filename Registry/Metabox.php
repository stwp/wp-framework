<?php

namespace Super\Registry;

use Super\Interfaces\DoneInterface;

class Metabox implements DoneInterface
{
    /**
     * @var string
     */
    protected $postType;

    protected $fields = [];

    public function __construct($postType)
    {
        $this->postType = $postType;

        Registry::add('metaboxes', $this->postType, $this);
    }

    public function addField($type, $id, $settings)
    {
        $this->fields[$id] = wp_parse_args([
            'type' => $type,
        ], $settings);

        return $this;
    }

    public function initMetaFields()
    {
        foreach ($this->fields as $fieldId => $field) {
            register_meta($this->postType, $fieldId, [
                'show_in_rest' => true,
                'single'       => true,
                'type'         => 'string',
            ]);
        }
    }

    public function inlineJson($hook_suffix)
    {
        if (in_array($hook_suffix, ['post.php', 'post-new.php'])) {
            $screen = get_current_screen();

            if (is_object($screen) && $this->postType == $screen->post_type) {
                $json = json_encode($this->fields);
                $js   = "var superPostMetaboxes = {$json}";

                wp_add_inline_script('super-builder-metaboxes', $js, 'before');
            }
        }
    }

    function editorAssets()
    {
        wp_enqueue_script('super-builder-metaboxes');
    }

    public function done()
    {
        add_action('init', [$this, 'initMetaFields']);
        add_action('admin_enqueue_scripts', [$this, 'inlineJson']);
        add_action('enqueue_block_editor_assets', [$this, 'editorAssets'], 11);
    }
}
