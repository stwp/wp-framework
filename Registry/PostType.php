<?php

namespace Super\Registry;

use Super\Exceptions\RegistryException;
use Super\Interfaces\DoneInterface;

class PostType implements DoneInterface
{
    /**
     * @var string The CPT name.
     */
    protected $singularName = '';

    /**
     * @var string The CPT name.
     */
    protected $pluralName = '';

    /**
     * @var bool Visibility in admin bar, menus, etc.
     */
    protected $public = true;

    /**
     * @var string
     */
    protected $icon = 'dashicons-screenoptions';

    /**
     * @var int
     */
    protected $position = 20;

    /**
     * @var array
     */
    protected $supports = ['title', 'thumbnail', 'editor'];

    /**
     * @var array Custom labels.
     */
    protected $labels = [];

    /**
     * @var array
     */
    protected $args = [];

    /*
     * @var string
     */
    protected $postType;

    /**
     * $this constructor.
     *
     * @param string $postType
     * @param string $singularName
     * @param string $pluralName
     *
     * @throws RegistryException
     */
    public function __construct($postType, $singularName = null, $pluralName = null)
    {
        $this->postType     = $postType;
        $this->singularName = $singularName ?: $this->postType;
        $this->pluralName   = $pluralName ?: $this->singularName;

        Registry::add('post-type', $this->postType, $this);
    }

    public function done()
    {
        $this->registerPostType();

        add_action('customize_register', [$this, 'registerCustomizerControls']);
        add_filter('super_post_type_link', [$this, 'archiveLink'], 99, 2);
    }

    /**
     * Register the CPT.
     *
     * @return void
     */
    protected function registerPostType()
    {
        $this->labels = wp_parse_args($this->labels, [
            'name'                  => $this->pluralName,
            'singular_name'         => $this->singularName,
            'insert_into_item'      => 'Insert image',
            'featured_image'        => 'Featured image',
            'set_featured_image'    => 'Set image',
            'remove_featured_image' => 'Remove image',
            'use_featured_image'    => 'Use as featured image',
        ]);

        $this->args = wp_parse_args($this->args, [
            'labels'              => $this->labels,
            'description'         => 'An object that includes all details about a ' . $this->singularName . ' type',
            'exclude_from_search' => false,
            'publicly_queryable'  => $this->public,
            'public'              => $this->public,
            'show_ui'             => true,
            'show_in_nav_menus'   => true,
            'show_in_menu'        => true,
            'show_in_rest'        => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => $this->position,
            'menu_icon'           => $this->icon,
            'taxonomies'          => [],
            'supports'            => $this->supports ? $this->supports : ['title'],
            'rewrite'             => [
                'slug' => str_replace('_', '-', $this->postType),
            ],
        ]);

        $type = [
            'name' => $this->postType,
            'args' => $this->args,
        ];

        register_post_type($type['name'], $type['args']);
    }

    /**
     * @param \WP_Customize_Manager $wp_customize
     */
    public function registerCustomizerControls($wp_customize)
    {
        $wp_customize->add_setting("super_cpt_{$this->postType}_page_id", [
            'capability' => 'edit_theme_options',
            'type'       => 'theme_mod',
        ]);

        $wp_customize->add_control("super_cpt_{$this->postType}_page_id", [
            'label'    => "{$this->labels['name']} page",
            'section'  => 'static_front_page',
            'type'     => 'dropdown-pages',
            'settings' => "super_cpt_{$this->postType}_page_id",
        ]);
    }

    public function archiveLink($link, $post_type)
    {
        if ($post_type != $this->postType) {
            return $link;
        }

        $pageId = get_theme_mod("super_cpt_{$this->postType}_page_id", 0);

        if (!empty($pageId)) {
            return get_the_permalink($pageId);
        }

        return $link;
    }

    /**
     * @param bool $public
     *
     * @return $this
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * @param string $icon
     *
     * @return $this
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @param int $position
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @param array $supports
     *
     * @return $this
     */
    public function setSupports($supports)
    {
        $this->supports = $supports;

        return $this;
    }

    /**
     * @param array $supports
     *
     * @alias for $this->setSupports
     *
     * @return $this
     */
    public function setSupport($supports)
    {
        $this->supports = $supports;

        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     *
     * @return $this
     */
    public function setLabel($key, $value)
    {
        $this->labels[$key] = $value;

        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     *
     * @return $this
     */
    public function setArg($key, $value)
    {
        $this->args[$key] = $value;

        return $this;
    }
}
