<?php

namespace Super\Registry;

use Super\Exceptions\RegistryException;
use Super\Interfaces\DoneInterface;

class Taxonomy implements DoneInterface
{
    /**
     * @var string The CPT name.
     */
    protected $singularName = '';

    /**
     * @var string The CPT name.
     */
    protected $pluralName = '';

    /**
     * @var array Custom labels.
     */
    protected $labels = [];

    /**
     * @var array
     */
    protected $args = [];

    /*
     * @var string
     */
    protected $taxonomy;

    /*
     * @var string|array
     */
    protected $postType;

    /**
     * $this constructor.
     *
     * @param string $taxonomy
     * @param string $postType
     * @param string $singularName
     * @param string $pluralName
     *
     * @throws RegistryException
     */
    public function __construct($taxonomy, $postType, $singularName = null, $pluralName = null)
    {
        $this->taxonomy     = $this->sanitizeId($taxonomy);
        $this->postType     = $postType;
        $this->singularName = $singularName ?: $this->taxonomy;
        $this->pluralName   = $pluralName ?: $this->singularName;

        Registry::add('taxonomy', $this->taxonomy, $this);
    }

    protected function sanitizeId($taxonomy)
    {
        return preg_replace('/\s/', '_', strtolower($taxonomy));
    }

    public function done()
    {
        $this->registerTaxonomy();
    }

    /**
     * Register the CPT.
     */
    protected function registerTaxonomy()
    {
        $this->labels = wp_parse_args($this->labels, [
            'name'                       => $this->pluralName,
            'singular_name'              => $this->singularName,
            'search_items'               => 'Search ' . $this->pluralName,
            'popular_items'              => 'Popular ' . $this->pluralName,
            'all_items'                  => 'All ' . $this->pluralName,
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => 'Edit item',
            'update_item'                => 'Update item',
            'add_new_item'               => 'Add New item',
            'new_item_name'              => 'New ' . $this->singularName . ' Name',
            'separate_items_with_commas' => 'Separate ' . $this->pluralName . ' with commas',
            'add_or_remove_items'        => 'Add or remove ' . $this->pluralName,
            'choose_from_most_used'      => 'Choose from the most used ' . $this->pluralName,
            'not_found'                  => 'No ' . $this->pluralName . ' found.',
            'menu_name'                  => $this->pluralName,
        ]);

        $this->args = wp_parse_args($this->args, [
            'hierarchical'          => true,
            'labels'                => $this->labels,
            'show_ui'               => true,
            'show_admin_column'     => true,
            'show_in_rest'          => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var'             => true,
        ]);

        register_taxonomy($this->taxonomy, $this->postType, $this->args);

        $postTypes = $this->postType;

        foreach ((array)$postTypes as $postType) {
            register_taxonomy_for_object_type($this->taxonomy, $postType);
        }
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setSingularName($value)
    {
        $this->labels['singular_name'] = $value;

        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setPluralName($value)
    {
        $this->labels['name'] = $value;

        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setName($value)
    {
        return $this->setPluralName($value);
    }

    /**
     * @param string $key
     * @param string $value
     *
     * @return $this
     */
    public function setLabel($key, $value)
    {
        $this->labels[$key] = $value;

        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     *
     * @return $this
     */
    public function setArg($key, $value)
    {
        $this->args[$key] = $value;

        return $this;
    }
}
