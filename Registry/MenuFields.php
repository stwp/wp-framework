<?php

namespace Super\Registry;

use Super\Interfaces\DoneInterface;
use Super\Support\MenuFieldsWalker;
use Timber\Timber;

class MenuFields implements DoneInterface
{
    /**
     * @var string
     */
    protected $menuId;

    /**
     * @var string
     */
    protected $fields = [];

    public function __construct()
    {
        if (!is_admin()) {
            return;
        }

        Registry::add('menu_fields', 'any', $this);
    }

    public function addField($type, $id, $settings)
    {
        $this->fields[$id] = wp_parse_args($settings, [
            'type'     => $type,
            'location' => false,
        ]);

        return $this;
    }

    public function saveMenu($id, $post)
    {
        if ($post->post_type !== 'nav_menu_item') {
            return $id;
        }

        $fields = Registry::get('menu_fields');

        if (isset($fields['any'])) {
            foreach ($fields['any']->fields as $name => $field) {
                if (isset($_POST['menu-item-' . $name][$id])) {
                    update_post_meta(
                        $id,
                        $name,
                        stripslashes($_POST['menu-item-' . $name][$id])
                    );
                }
            }
        }

        return $id;
    }

    static function getFormattedFields($item)
    {
        $new_fields = '';

        $fields = Registry::get('menu_fields');

        if (isset($fields['any'])) {
            foreach ($fields['any']->fields as $name => $field) {
                $field_class = is_string($field['location']) ? 'super-menu-field-for-' . sanitize_html_class($field['location']) : '';

                /* @var string|array $field_location */
                $field_location = $field['location'];

                if (empty($field_class) && is_array($field_location)) {
                    $field_class = join(' ', array_map(function ($location) {
                        return 'super-menu-field-for-' . sanitize_html_class($location);
                    }, $field_location));
                }

                $new_fields .= Timber::fetch('admin/menu-fields.twig', wp_parse_args([
                    'name'        => $name,
                    'id'          => $item->ID,
                    'value'       => get_post_meta($item->ID, $name, true),
                    'field_class' => $field_class,
                ], $field), false);
            }
        }

        return $new_fields;
    }

    public function adminEnqueue(){
        wp_enqueue_media();
    }

    public function done()
    {
        add_action('admin_enqueue_scripts', [$this, 'adminEnqueue'], 5);
        add_filter('wp_edit_nav_menu_walker', function () {
            return MenuFieldsWalker::class;
        });
        add_action('save_post', [$this, 'saveMenu'], 10, 2);
    }
}
