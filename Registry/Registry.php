<?php

namespace Super\Registry;

use Super\Exceptions\RegistryException;

class Registry
{
    private static $data = [];

    private function __construct()
    {
    }

    /**
     * @param $registryType
     * @param $registryId
     * @param $configuration
     *
     * @throws RegistryException
     */
    public static function add($registryType, $registryId, $configuration)
    {
        if (isset(self::$data[$registryType][$registryId])) {
            throw new RegistryException("{$registryId} is already registered");
        }

        self::$data[$registryType][$registryId] = $configuration;
    }

    /**
     * Check if an object like this is already registered.
     *
     * @param $registryType
     *
     * @return bool
     */
    public static function has($registryType)
    {
        return array_key_exists($registryType, self::$data);
    }

    /**
     * Get all objects from a registry
     *
     * @param $registryType
     *
     * @return mixed|null
     */
    public static function get($registryType)
    {
        if (self::has($registryType)) {
            return self::$data[$registryType];
        }

        return null;
    }

    /**
     * Get all data.
     *
     * @return array
     */
    public static function all()
    {
        return self::$data;
    }

}
