<?php

namespace Super\Interfaces;

interface DoneInterface
{
    public function done();
}
