<?php

namespace Super\Interfaces;
/**
 * ElementInterface
 *
 * Minimum requirements for objects like posts, taxonomy terms, etc.
 *
 * @package Super\Interfaces
 */
interface ElementInterface
{
    public function id();

    public function title();

    public function url();
}
