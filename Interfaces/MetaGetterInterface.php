<?php

namespace Super\Interfaces;

interface MetaGetterInterface
{
    public function meta($key);

    public function get_field($key);
}
